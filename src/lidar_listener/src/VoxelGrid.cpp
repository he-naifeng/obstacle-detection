#pragma once

#include <ros/ros.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>

#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/segmentation/conditional_euclidean_clustering.h>

#include <pcl/common/common.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/search/organized.h>
#include <pcl/search/kdtree.h>

#include <std_msgs/Header.h>

#include <jsk_recognition_msgs/BoundingBox.h>
#include <jsk_recognition_msgs/BoundingBoxArray.h>

#include <sensor_msgs/PointCloud2.h>
#include<pcl/filters/statistical_outlier_removal.h>   //统计滤波器头文件
#include <pcl/filters/crop_box.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/bilateral.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/bilateral.h>
#include <pcl/filters/convolution_3d.h>
#include <pcl/filters/convolution.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h> 
#include <unordered_set>


using namespace std;

class talker_listener
{
private:

    int num;

    ros::NodeHandle nh;
    ros::Subscriber sub;
    ros::Publisher pub;
    
public:
    talker_listener()
    {
        num = 100;

        // sub = nh.subscribe<sensor_msgs::PointCloud2>("os_cloud_node/points",1,Callback,this);
        sub = nh.subscribe("/os_cloud_node/points", 5, &talker_listener::Callback, this);
       
        pub = nh.advertise<sensor_msgs::PointCloud2>("/chatter1", 5);

    }

    void Callback(const sensor_msgs::PointCloud2ConstPtr &in_cloud_ptr);

    void voxelgrid(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_voxelgrid, float length, float width, float heigth);

    void passthrough_x(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_PassThrough,float rangelow, float rangehigh);

    void passthrough_y(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_PassThrough,float rangelow, float rangehigh);
  
    void passthrough_z(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_PassThrough,float rangelow, float rangehigh);

    void statisticalOutlierRemoval(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_statisticalFilter);

    void radius_outlier_removal(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_statisticalFilter);

};

void talker_listener::voxelgrid(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_voxelgrid,
                                                                 float length, float width, float heigth) 
{
    //方法2：体素滤波器实现下采样
	pcl::VoxelGrid<pcl::PointXYZ> voxelgrid;
	voxelgrid.setInputCloud(cloud);//输入点云数据
	voxelgrid.setLeafSize(length, width, heigth);//AABB长宽高
	voxelgrid.filter(*cloud_after_voxelgrid);
}

void talker_listener::passthrough_x(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_PassThrough,
                                                                        float rangelow, float rangehigh)
{
    //方法1：直通滤波器对点云处理
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_PassThrough(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PassThrough<pcl::PointXYZ> passthrough;
	passthrough.setInputCloud(cloud);//输入点云
	passthrough.setFilterFieldName("x");//对x轴进行操作
	passthrough.setFilterLimits(rangelow, rangehigh);//设置直通滤波器操作范围
	//passthrough.setFilterLimitsNegative(true);//true表示保留范围内，false表示保留范围外
	passthrough.filter(*cloud_after_PassThrough);//执行滤波，过滤结果保存在 cloud_after_PassThrough
}

void talker_listener::passthrough_y(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_PassThrough,
                                                                        float rangelow, float rangehigh)
{
    //方法1：直通滤波器对点云处理
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_PassThrough(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PassThrough<pcl::PointXYZ> passthrough;
	passthrough.setInputCloud(cloud);//输入点云
	passthrough.setFilterFieldName("y");//对y轴进行操作
	passthrough.setFilterLimits(rangelow, rangehigh);//设置直通滤波器操作范围
	//passthrough.setFilterLimitsNegative(true);//true表示保留范围内，false表示保留范围外
	passthrough.filter(*cloud_after_PassThrough);//执行滤波，过滤结果保存在 cloud_after_PassThrough
}

void talker_listener::passthrough_z(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_PassThrough,
                                                                        float rangelow, float rangehigh)
{
    //方法1：直通滤波器对点云处理
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_PassThrough(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PassThrough<pcl::PointXYZ> passthrough;
	passthrough.setInputCloud(cloud);//输入点云
	passthrough.setFilterFieldName("z");//对z轴进行操作
	passthrough.setFilterLimits(rangelow, rangehigh);//设置直通滤波器操作范围
	//passthrough.setFilterLimitsNegative(true);//true表示保留范围内，false表示保留范围外
	passthrough.filter(*cloud_after_PassThrough);//执行滤波，过滤结果保存在 cloud_after_PassThrough
}

void talker_listener::statisticalOutlierRemoval(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_statisticalFilter)
{
    //  创建滤波器对象
  pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
  sor.setInputCloud (cloud);
  sor.setMeanK (100);//设置在进行统计时考虑查询点邻近点数
  sor.setStddevMulThresh (1.0);//设置判断是否为离群点的倒值,x = 1.0, t = mean+/-(stddev*x)
  sor.filter (*cloud_after_statisticalFilter);
}

void talker_listener::radius_outlier_removal(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_radius)
{
	pcl::RadiusOutlierRemoval<pcl::PointXYZ> sor;
	sor.setInputCloud(cloud);
	sor.setRadiusSearch(0.02);
	sor.setMinNeighborsInRadius(2);
	sor.setNegative(false); 
	sor.filter(*cloud_after_radius);  
}


void talker_listener::Callback(const sensor_msgs::PointCloud2ConstPtr &in_cloud_ptr)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr current_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr voxelgrid_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr passthrough_pc_ptr_x(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr passthrough_pc_ptr_y(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr passthrough_pc_ptr_z(new pcl::PointCloud<pcl::PointXYZ>);

    pcl::PointCloud<pcl::PointXYZ>::Ptr gaussian_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr statistical_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr radius_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_p(new pcl::PointCloud<pcl::PointXYZ>);

    pcl::fromROSMsg(*in_cloud_ptr, *current_pc_ptr);
    passthrough_x(current_pc_ptr,passthrough_pc_ptr_x,0,3);
    passthrough_y(passthrough_pc_ptr_x,passthrough_pc_ptr_y,-2,2);
    passthrough_z(passthrough_pc_ptr_y,passthrough_pc_ptr_z,0,2);

    statisticalOutlierRemoval(passthrough_pc_ptr_z,statistical_pc_ptr);
    radius_outlier_removal(statistical_pc_ptr,radius_pc_ptr);
    // voxelgrid(radius_pc_ptr,voxelgrid_pc_ptr,0.05,0.05,0.05);
    voxelgrid(radius_pc_ptr,voxelgrid_pc_ptr,0.01,0.01,0.01);

    sensor_msgs::PointCloud2 cloud_msg;
    pcl::toROSMsg(*voxelgrid_pc_ptr, cloud_msg);
    cloud_msg.header = in_cloud_ptr->header;
    pub.publish(cloud_msg);

}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "VoxelGrid");

    talker_listener tl;

    ros::spin();

    return 0;
}
