#pragma once

#include <ros/ros.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>

#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/segmentation/conditional_euclidean_clustering.h>

#include <pcl/common/common.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/search/organized.h>
#include <pcl/search/kdtree.h>

#include <std_msgs/Header.h>

#include <jsk_recognition_msgs/BoundingBox.h>
#include <jsk_recognition_msgs/BoundingBoxArray.h>

#include <sensor_msgs/PointCloud2.h>
#include<pcl/filters/statistical_outlier_removal.h>   //统计滤波器头文件
#include <pcl/filters/crop_box.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/bilateral.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/bilateral.h>
#include <pcl/filters/convolution_3d.h>
#include <pcl/filters/convolution.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h> 
#include <unordered_set>
#include <pcl/filters/crop_hull.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/crop_hull.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <iostream>


using namespace std;

class talker_listener
{
private:

    int num;

    ros::NodeHandle nh;
    ros::Subscriber sub;
    ros::Publisher pub;
    
public:
    talker_listener()
    {
        num = 100;

        // sub = nh.subscribe<sensor_msgs::PointCloud2>("os_cloud_node/points",1,Callback,this);
        sub = nh.subscribe("/os_cloud_node/points", 5, &talker_listener::Callback, this);
       
        pub = nh.advertise<sensor_msgs::PointCloud2>("/chatter1", 5);

    }

    void Callback(const sensor_msgs::PointCloud2ConstPtr &in_cloud_ptr);

    void voxelgrid(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_voxelgrid);

    void crophull(pcl::PointCloud<pcl::PointXYZ>::Ptr msg,pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_crophull);

    void preprocess(pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloud,pcl::PointCloud<pcl::PointXYZ>::Ptr outputFile);
};


// *crophull滤波*/
void talker_listener::crophull(pcl::PointCloud<pcl::PointXYZ>::Ptr msg,pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_crophull)
// pcl::PointCloud<pcl::PointXYZ>::Ptr crophull(pcl::PointCloud<pcl::PointXYZ>::Ptr msg)
{
    /*设置封闭范围顶点*/
    pcl::PointCloud<pcl::PointXYZ>::Ptr boundingbox_ptr (new pcl::PointCloud<pcl::PointXYZ>);
    boundingbox_ptr->push_back(pcl::PointXYZ(0.1, 0.1, 0.1));
    boundingbox_ptr->push_back(pcl::PointXYZ(0.1, -0.1,0.1 ));
    boundingbox_ptr->push_back(pcl::PointXYZ(-0.1, 0.1,0.1 ));

    /*构造凸包*/
    pcl::ConvexHull<pcl::PointXYZ> hull;
	hull.setInputCloud(boundingbox_ptr);
	hull.setDimension(2);
	std::vector<pcl::Vertices> polygons;
	pcl::PointCloud<pcl::PointXYZ>::Ptr surface_hull (new pcl::PointCloud<pcl::PointXYZ>);
	hull.reconstruct(*surface_hull, polygons);

    /*crophull滤波*/
	pcl::PointCloud<pcl::PointXYZ>::Ptr objects (new pcl::PointCloud<pcl::PointXYZ>);
    
	pcl::CropHull<pcl::PointXYZ> bb_filter;
	bb_filter.setDim(2);
	bb_filter.setInputCloud(msg);
	bb_filter.setHullIndices(polygons);
	bb_filter.setHullCloud(surface_hull);
	bb_filter.filter(*cloud_after_crophull);
    // return objects;
}

/*体素滤波*/
void talker_listener::voxelgrid(pcl::PointCloud<pcl::PointXYZ>::Ptr msg, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_after_voxelgrid)
// pcl::PointCloud<pcl::PointXYZ>::Ptr voxelGrid(pcl::PointCloud<pcl::PointXYZ>::Ptr msg)
{
    /*进行体素滤波*/ 
    /*定义指针时一定要记得分配指向的内存空间*/
    // pcl::PointCloud<pcl::PointXYZ>::Ptr voxelgrid_filtered(new pcl::PointCloud<pcl::PointXYZ>);  
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    sor.setInputCloud (msg);
    sor.setLeafSize (0.4, 0.4, 0.4);
    // apply filter
    sor.filter (*cloud_after_voxelgrid);
    // return voxelgrid_filtered;
}

/*对三维点云实现预处理*/
void talker_listener::preprocess(pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloud,pcl::PointCloud<pcl::PointXYZ>::Ptr outputFile)
{


    /*直通滤波*/
    /*
    pcl::PCLPointCloud2* passthrough_filtered = new pcl::PCLPointCloud2; 
    *passthrough_filtered = passthrough(cloud);*/
    
    /*CropHull滤波*/
    pcl::PointCloud<pcl::PointXYZ>::Ptr crophull_output(new pcl::PointCloud<pcl::PointXYZ>);
    ROS_INFO("inputCloud:%zd",inputCloud->size());
    crophull(inputCloud,crophull_output);
    
    /*统计学滤波*/
    /*有过一次测试，在CropHull滤波之后，在进行统计学滤波容易产生空洞*/
    //pcl::PointCloud<pcl::PointXYZ>::Ptr statistical_output(new pcl::PointCloud<pcl::PointXYZ>);
    //statistical_output = statisticalRemoval(crophull_output);
    
    /*体素滤波*/
    // pcl::PointCloud<pcl::PointXYZ>::Ptr voxelgrid_output(new pcl::PointCloud<pcl::PointXYZ>);
    ROS_INFO("crophull_output:%zd",crophull_output->size());
    // voxelgrid(crophull_output,outputFile); 
    outputFile = crophull_output ;
    ROS_INFO("outputFile:%zd",outputFile->size());

    
    // /*保存滤波后的点云文件*/
    // pcl::PointCloud<pcl::PointXYZ>::Ptr &output = voxelgrid_output;
    // pcl::io::savePCDFileASCII(outputFile, *output);
    // cout<<"pointcloud_filtered size = "<<output->points.size()<<endl;
} 



void talker_listener::Callback(const sensor_msgs::PointCloud2ConstPtr &in_cloud_ptr)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr current_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr voxelgrid_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr passthrough_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr gaussian_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr statistical_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr radius_pc_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg(*in_cloud_ptr, *current_pc_ptr);

    // voxelgrid(current_pc_ptr,voxelgrid_pc_ptr,0.1,0.1,0.1);
    // passthrough(voxelgrid_pc_ptr,passthrough_pc_ptr,0,2);
    // statisticalOutlierRemoval(passthrough_pc_ptr,statistical_pc_ptr);
    // radius_outlier_removal(statistical_pc_ptr,radius_pc_ptr);
    pcl::PointCloud<pcl::PointXYZ>::Ptr outputFile(new pcl::PointCloud<pcl::PointXYZ>);
    ROS_INFO("current:%zd",current_pc_ptr->size());
    preprocess(current_pc_ptr,outputFile);

    sensor_msgs::PointCloud2 cloud_msg;
    pcl::toROSMsg(*outputFile, cloud_msg);
    cloud_msg.header = in_cloud_ptr->header;
    
    // ROS_INFO("current:%zd",current_pc_ptr->size());
    // ROS_INFO("voxelgrid:%zd",voxelgrid_pc_ptr->size());
    // ROS_INFO("passthrough:%zd",passthrough_pc_ptr->size());
    // ROS_INFO("statistical:%zd",statistical_pc_ptr->size());
    // ROS_INFO("radius_pc_ptr:%zd",radius_pc_ptr->size());
    pub.publish(cloud_msg);

}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "lidar_listener");

    talker_listener tl;

    ros::spin();

    return 0;
}
