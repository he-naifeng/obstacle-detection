;; Auto-generated. Do not edit!


(when (boundp 'euclidean_cluster::points)
  (if (not (find-package "EUCLIDEAN_CLUSTER"))
    (make-package "EUCLIDEAN_CLUSTER"))
  (shadow 'points (find-package "EUCLIDEAN_CLUSTER")))
(unless (find-package "EUCLIDEAN_CLUSTER::POINTS")
  (make-package "EUCLIDEAN_CLUSTER::POINTS"))

(in-package "ROS")
;;//! \htmlinclude points.msg.html


(defclass euclidean_cluster::points
  :super ros::object
  :slots (_max_x _min_x _max_y _min_y _x _y ))

(defmethod euclidean_cluster::points
  (:init
   (&key
    ((:max_x __max_x) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:min_x __min_x) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:max_y __max_y) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:min_y __min_y) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:x __x) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:y __y) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _max_x __max_x)
   (setq _min_x __min_x)
   (setq _max_y __max_y)
   (setq _min_y __min_y)
   (setq _x __x)
   (setq _y __y)
   self)
  (:max_x
   (&optional __max_x)
   (if __max_x (setq _max_x __max_x)) _max_x)
  (:min_x
   (&optional __min_x)
   (if __min_x (setq _min_x __min_x)) _min_x)
  (:max_y
   (&optional __max_y)
   (if __max_y (setq _max_y __max_y)) _max_y)
  (:min_y
   (&optional __min_y)
   (if __min_y (setq _min_y __min_y)) _min_y)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:serialization-length
   ()
   (+
    ;; float64[] _max_x
    (* 8    (length _max_x)) 4
    ;; float64[] _min_x
    (* 8    (length _min_x)) 4
    ;; float64[] _max_y
    (* 8    (length _max_y)) 4
    ;; float64[] _min_y
    (* 8    (length _min_y)) 4
    ;; float64[] _x
    (* 8    (length _x)) 4
    ;; float64[] _y
    (* 8    (length _y)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[] _max_x
     (write-long (length _max_x) s)
     (dotimes (i (length _max_x))
       (sys::poke (elt _max_x i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _min_x
     (write-long (length _min_x) s)
     (dotimes (i (length _min_x))
       (sys::poke (elt _min_x i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _max_y
     (write-long (length _max_y) s)
     (dotimes (i (length _max_y))
       (sys::poke (elt _max_y i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _min_y
     (write-long (length _min_y) s)
     (dotimes (i (length _min_y))
       (sys::poke (elt _min_y i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _x
     (write-long (length _x) s)
     (dotimes (i (length _x))
       (sys::poke (elt _x i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _y
     (write-long (length _y) s)
     (dotimes (i (length _y))
       (sys::poke (elt _y i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[] _max_x
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _max_x (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _max_x i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _min_x
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _min_x (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _min_x i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _max_y
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _max_y (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _max_y i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _min_y
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _min_y (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _min_y i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _x
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _x (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _x i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _y
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _y (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _y i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;;
   self)
  )

(setf (get euclidean_cluster::points :md5sum-) "7b7a87df9978bbd6dacb327d7383103c")
(setf (get euclidean_cluster::points :datatype-) "euclidean_cluster/points")
(setf (get euclidean_cluster::points :definition-)
      "float64[] max_x
float64[] min_x
float64[] max_y
float64[] min_y
float64[] x
float64[] y

")



(provide :euclidean_cluster/points "7b7a87df9978bbd6dacb327d7383103c")


