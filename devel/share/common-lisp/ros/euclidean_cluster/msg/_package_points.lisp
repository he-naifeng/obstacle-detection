(cl:in-package euclidean_cluster-msg)
(cl:export '(MAX_X-VAL
          MAX_X
          MIN_X-VAL
          MIN_X
          MAX_Y-VAL
          MAX_Y
          MIN_Y-VAL
          MIN_Y
          X-VAL
          X
          Y-VAL
          Y
))