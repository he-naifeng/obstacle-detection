// Auto-generated. Do not edit!

// (in-package euclidean_cluster.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class points {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.max_x = null;
      this.min_x = null;
      this.max_y = null;
      this.min_y = null;
      this.x = null;
      this.y = null;
    }
    else {
      if (initObj.hasOwnProperty('max_x')) {
        this.max_x = initObj.max_x
      }
      else {
        this.max_x = [];
      }
      if (initObj.hasOwnProperty('min_x')) {
        this.min_x = initObj.min_x
      }
      else {
        this.min_x = [];
      }
      if (initObj.hasOwnProperty('max_y')) {
        this.max_y = initObj.max_y
      }
      else {
        this.max_y = [];
      }
      if (initObj.hasOwnProperty('min_y')) {
        this.min_y = initObj.min_y
      }
      else {
        this.min_y = [];
      }
      if (initObj.hasOwnProperty('x')) {
        this.x = initObj.x
      }
      else {
        this.x = [];
      }
      if (initObj.hasOwnProperty('y')) {
        this.y = initObj.y
      }
      else {
        this.y = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type points
    // Serialize message field [max_x]
    bufferOffset = _arraySerializer.float64(obj.max_x, buffer, bufferOffset, null);
    // Serialize message field [min_x]
    bufferOffset = _arraySerializer.float64(obj.min_x, buffer, bufferOffset, null);
    // Serialize message field [max_y]
    bufferOffset = _arraySerializer.float64(obj.max_y, buffer, bufferOffset, null);
    // Serialize message field [min_y]
    bufferOffset = _arraySerializer.float64(obj.min_y, buffer, bufferOffset, null);
    // Serialize message field [x]
    bufferOffset = _arraySerializer.float64(obj.x, buffer, bufferOffset, null);
    // Serialize message field [y]
    bufferOffset = _arraySerializer.float64(obj.y, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type points
    let len;
    let data = new points(null);
    // Deserialize message field [max_x]
    data.max_x = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [min_x]
    data.min_x = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [max_y]
    data.max_y = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [min_y]
    data.min_y = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [x]
    data.x = _arrayDeserializer.float64(buffer, bufferOffset, null)
    // Deserialize message field [y]
    data.y = _arrayDeserializer.float64(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.max_x.length;
    length += 8 * object.min_x.length;
    length += 8 * object.max_y.length;
    length += 8 * object.min_y.length;
    length += 8 * object.x.length;
    length += 8 * object.y.length;
    return length + 24;
  }

  static datatype() {
    // Returns string type for a message object
    return 'euclidean_cluster/points';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '7b7a87df9978bbd6dacb327d7383103c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64[] max_x
    float64[] min_x
    float64[] max_y
    float64[] min_y
    float64[] x
    float64[] y
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new points(null);
    if (msg.max_x !== undefined) {
      resolved.max_x = msg.max_x;
    }
    else {
      resolved.max_x = []
    }

    if (msg.min_x !== undefined) {
      resolved.min_x = msg.min_x;
    }
    else {
      resolved.min_x = []
    }

    if (msg.max_y !== undefined) {
      resolved.max_y = msg.max_y;
    }
    else {
      resolved.max_y = []
    }

    if (msg.min_y !== undefined) {
      resolved.min_y = msg.min_y;
    }
    else {
      resolved.min_y = []
    }

    if (msg.x !== undefined) {
      resolved.x = msg.x;
    }
    else {
      resolved.x = []
    }

    if (msg.y !== undefined) {
      resolved.y = msg.y;
    }
    else {
      resolved.y = []
    }

    return resolved;
    }
};

module.exports = points;
