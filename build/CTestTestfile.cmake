# CMake generated Testfile for 
# Source directory: /home/he/catkin_ws/src
# Build directory: /home/he/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("lidar_start")
subdirs("lidar_listener")
subdirs("euclidean_cluster")
subdirs("pcl_test")
