# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "euclidean_cluster: 1 messages, 0 services")

set(MSG_I_FLAGS "-Ieuclidean_cluster:/home/he/catkin_ws/src/euclidean_cluster/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(euclidean_cluster_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg" NAME_WE)
add_custom_target(_euclidean_cluster_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "euclidean_cluster" "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(euclidean_cluster
  "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/euclidean_cluster
)

### Generating Services

### Generating Module File
_generate_module_cpp(euclidean_cluster
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/euclidean_cluster
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(euclidean_cluster_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(euclidean_cluster_generate_messages euclidean_cluster_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg" NAME_WE)
add_dependencies(euclidean_cluster_generate_messages_cpp _euclidean_cluster_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(euclidean_cluster_gencpp)
add_dependencies(euclidean_cluster_gencpp euclidean_cluster_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS euclidean_cluster_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(euclidean_cluster
  "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/euclidean_cluster
)

### Generating Services

### Generating Module File
_generate_module_eus(euclidean_cluster
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/euclidean_cluster
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(euclidean_cluster_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(euclidean_cluster_generate_messages euclidean_cluster_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg" NAME_WE)
add_dependencies(euclidean_cluster_generate_messages_eus _euclidean_cluster_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(euclidean_cluster_geneus)
add_dependencies(euclidean_cluster_geneus euclidean_cluster_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS euclidean_cluster_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(euclidean_cluster
  "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/euclidean_cluster
)

### Generating Services

### Generating Module File
_generate_module_lisp(euclidean_cluster
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/euclidean_cluster
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(euclidean_cluster_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(euclidean_cluster_generate_messages euclidean_cluster_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg" NAME_WE)
add_dependencies(euclidean_cluster_generate_messages_lisp _euclidean_cluster_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(euclidean_cluster_genlisp)
add_dependencies(euclidean_cluster_genlisp euclidean_cluster_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS euclidean_cluster_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(euclidean_cluster
  "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/euclidean_cluster
)

### Generating Services

### Generating Module File
_generate_module_nodejs(euclidean_cluster
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/euclidean_cluster
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(euclidean_cluster_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(euclidean_cluster_generate_messages euclidean_cluster_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg" NAME_WE)
add_dependencies(euclidean_cluster_generate_messages_nodejs _euclidean_cluster_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(euclidean_cluster_gennodejs)
add_dependencies(euclidean_cluster_gennodejs euclidean_cluster_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS euclidean_cluster_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(euclidean_cluster
  "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/euclidean_cluster
)

### Generating Services

### Generating Module File
_generate_module_py(euclidean_cluster
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/euclidean_cluster
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(euclidean_cluster_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(euclidean_cluster_generate_messages euclidean_cluster_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/he/catkin_ws/src/euclidean_cluster/msg/points.msg" NAME_WE)
add_dependencies(euclidean_cluster_generate_messages_py _euclidean_cluster_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(euclidean_cluster_genpy)
add_dependencies(euclidean_cluster_genpy euclidean_cluster_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS euclidean_cluster_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/euclidean_cluster)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/euclidean_cluster
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(euclidean_cluster_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/euclidean_cluster)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/euclidean_cluster
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(euclidean_cluster_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/euclidean_cluster)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/euclidean_cluster
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(euclidean_cluster_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/euclidean_cluster)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/euclidean_cluster
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(euclidean_cluster_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/euclidean_cluster)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/euclidean_cluster\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/euclidean_cluster
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(euclidean_cluster_generate_messages_py std_msgs_generate_messages_py)
endif()
