# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/he/catkin_ws/devel/include;/home/he/catkin_ws/src/euclidean_cluster/include".split(';') if "/home/he/catkin_ws/devel/include;/home/he/catkin_ws/src/euclidean_cluster/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;sensor_msgs;pcl_ros;std_msgs;jsk_recognition_msgs;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "euclidean_cluster"
PROJECT_SPACE_DIR = "/home/he/catkin_ws/devel"
PROJECT_VERSION = "0.0.1"
