set(_CATKIN_CURRENT_PACKAGE "lidar_start")
set(lidar_start_VERSION "0.0.0")
set(lidar_start_MAINTAINER "he <he@todo.todo>")
set(lidar_start_PACKAGE_FORMAT "2")
set(lidar_start_BUILD_DEPENDS "roscpp" "rospy" "std_msgs")
set(lidar_start_BUILD_EXPORT_DEPENDS "roscpp" "rospy" "std_msgs")
set(lidar_start_BUILDTOOL_DEPENDS "catkin")
set(lidar_start_BUILDTOOL_EXPORT_DEPENDS )
set(lidar_start_EXEC_DEPENDS "roscpp" "rospy" "std_msgs")
set(lidar_start_RUN_DEPENDS "roscpp" "rospy" "std_msgs")
set(lidar_start_TEST_DEPENDS )
set(lidar_start_DOC_DEPENDS )
set(lidar_start_URL_WEBSITE "")
set(lidar_start_URL_BUGTRACKER "")
set(lidar_start_URL_REPOSITORY "")
set(lidar_start_DEPRECATED "")